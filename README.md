# Horovod benchmarks on the Piz Daint supercomputer

This is a fork of [alsrgv/benchmarks](https://github.com/alsrgv/benchmarks) including
scripts to run Horovod benchmarks on the CSCS Piz Daint supercomputer.

## Prerequisites

TensorFlow has to be available as an [EasyBuild module](https://user.cscs.ch/scientific_computing/code_compilation/easybuild_framework/).
This can be achieved by building it using the provided EasyBuild configuration
on Piz Daint.

The ImageNet dataset has to be [prepared in the TFRecord format](https://github.com/tensorflow/models/blob/master/research/inception/inception/data/download_and_preprocess_imagenet.sh). 

The file config.sh has to be edited:

* `MODULEDIR` points to the directory containing the EasyBuild modules (i.e. TensorFlow)
* `DATADIR` points to the directory containing the preprocessed ImageNet dataset

Horovod has to be installed. To install horovod on Piz Daint, run `setup_virtenv_daint.sh`. This installs
horovod in a virtual environment.

To run the benchmarks, use `scheduleall.sh`.

# Instructions for adding distributed benchmarks to continuous run:

1. You can add your benchmark file under
   [tensorflow/benchmarks/models](https://github.com/tensorflow/benchmarks/tree/master/models) directory. The benchmark should accept `task_id`, `job_name`, `ps_hosts` and `worker_hosts` flags. You can copy-paste the following flag definitions:

    ```python
    tf.app.flags.DEFINE_integer("task_id", None, "Task index, should be >= 0.")
    tf.app.flags.DEFINE_string("job_name", None, "job name: worker or ps")
    tf.app.flags.DEFINE_string("ps_hosts", None, "Comma-separated list of hostname:port pairs")
    tf.app.flags.DEFINE_string("worker_hosts", None, "Comma-separated list of hostname:port pairs")
    ```
2. Report benchmark values by calling `store_data_in_json` from your benchmark
   code. This function is defined in
   [benchmark\_util.py](https://github.com/tensorflow/benchmarks/blob/master/models/util/benchmark_util.py)
3. Create a Dockerfile that sets up dependencies and runs your benchmark. For
   example, see [Dockerfile.alexnet\_distributed\_test](https://github.com/tensorflow/benchmarks/blob/master/models/Dockerfile.alexnet_distributed_test)
4. Add the benchmark to
   [benchmark\_configs.yml](https://github.com/tensorflow/benchmarks/blob/master/scripts/benchmark_configs.yml)
   * Set `benchmark_name` to a descriptive name for your benchmark and make sure
     it is unique.
   * Set `worker_count` and `ps_count`.
   * Set `docker_file` to the Dockerfile path starting with `benchmarks/`
     directory.
   * Optionally, you can pass flags to your benchmark by adding `args` list.
5. Send PR with the changes to annarev.

For any questions, please contact annarev@google.com.
