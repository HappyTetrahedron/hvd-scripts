#!/bin/bash

for i in 1 2 4 8 16 32
do
  echo "Scheduling job for ${i} nodes: "
for net in inception3 alexnet resnet50 resnet152 vgg19
  do 
    echo -n "  $net... "
    sbatch -N ${i} google-benchmarks_dist_daint.sh ${i} ${i} horovod true $net 32
  done
done
