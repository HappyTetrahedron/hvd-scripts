#!/bin/bash

#SBATCH --job-name=google_benchmark
#SBATCH --time=00:12:00
#SBATCH --constraint=gpu
#SBATCH --output=dist_benchmark_daint.%j.log

# Arguments:
#   $1: TF_NUM_PS: number of parameter servers
#   $2: TF_NUM_WORKER: number of workers
#   $3: variable_update: parameter_server/distributed_replicated
#   $4: real_data: true/false
#   $5: model: Name of model to train
#   $6: batch_size: batch size per node

source config.sh

# load modules
module use "$MODULEDIR"
module load daint-gpu
module unload CrayGNU
module load TensorFlow/1.2.1-CrayGNU-17.08-cuda-8.0-python3

# load virtualenv
export WORKON_HOME=~/Envs
source $WORKON_HOME/tf_horo/bin/activate

# set TensorFlow script parameters
export TF_SCRIPT="$(pwd)/scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py"

data_flags="
--num_gpus=1 \
--batch_size=$6 \
--display_every=1 \
--use_nccl=True \
--data_format=NCHW \
--variable_update=$3 \
--local_parameter_device=gpu \
--device=gpu \
--horovod_device=gpu \
--optimizer=sgd \
--model=$5 \
--data_name=imagenet \
--data_dir=$DATADIR
"
nodata_flags="
--num_gpus=1 \
--batch_size=$6 \
--display_every=1 \
--data_format=NCHW \
--use_nccl=True \
--variable_update=$3 \
--local_parameter_device=gpu \
--optimizer=sgd \
--model=$5 \
--data_name=imagenet
"
if [ "$4" = "true" ]; then
  export TF_FLAGS=$data_flags
elif [ "$4" = "false" ]; then
  export TF_FLAGS=$nodata_flags
else
  echo "error in real_data argument"
  exit 1
fi

# set TensorFlow distributed parameters
export TF_NUM_PS=$1
export TF_NUM_WORKERS=$2 # $SLURM_JOB_NUM_NODES
# export TF_WORKER_PER_NODE=1
# export TF_PS_PER_NODE=1
# export TF_PS_IN_WORKER=true
export TF_MODEL=$5
echo $TF_MODEL

# run distributed TensorFlow
DIST_TF_LAUNCHER_DIR=$SCRATCH/hvd
mkdir -p $DIST_TF_LAUNCHER_DIR/$5/$2
cd $DIST_TF_LAUNCHER_DIR/$5/$2

srun -N $SLURM_JOB_NUM_NODES python3 $TF_SCRIPT $TF_FLAGS

# deactivate virtualenv
deactivate
