#!/usr/bin/env bash

source config.sh

# load modules
module use "$MODULEDIR"
module load daint-gpu
module load TensorFlow/1.2.1-CrayGNU-17.08-cuda-8.0-python3

# create virtualenv
export WORKON_HOME=~/Envs
mkdir -p $WORKON_HOME
cd $WORKON_HOME
virtualenv tf-daint

# load virtualenv
source $WORKON_HOME/tf-daint/bin/activate

# install dependencies
pip install -r requirements.txt

# install aws-cli
pip install awscli

pip install horovod

# deactivate virtualenv
deactivate
